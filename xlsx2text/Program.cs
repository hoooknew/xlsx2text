﻿using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.IO;
using System.Linq;

namespace xlsx2text
{
    class Program
    {
        static void Main(string[] args)
        {
            var stdout = Console.Out;

            using (var stdin = Console.OpenStandardInput())
            {
                var data = new MemoryStream();
                stdin.CopyTo(data);
                data.Position = 0;

                var pkg = new ExcelPackage(data);
                var book = pkg.Workbook;

                var sheetNamePadding = book.Worksheets.Select(r => r.Name.Length).Max();

                var firstSheet = true;
                foreach (var sheet in book.Worksheets)
                {
                    if (firstSheet)
                        firstSheet = false;
                    else
                        stdout.WriteLine();

                    var dim = 
                        (
                            StartRow :sheet.Dimension.Start.Row, 
                            StartCol : sheet.Dimension.Start.Column, 
                            EndRow : sheet.Dimension.End.Row, 
                            EndCol : sheet.Dimension.End.Column
                        );
                    
                    var addrPad = dim.EndRow.ToString().Length + dim.StartCol.ToString().Length+ 2;

                    var columnWidths = Enumerable.Range(dim.StartCol, dim.EndCol)
                        .ToDictionary(c => c, c => Enumerable.Range(dim.StartRow, dim.EndRow).Select(r => GetCellText(sheet.Cells[r, c]).Length).Max());

                    for (int r = dim.StartRow; r <= dim.EndRow; r++)
                    {
                        stdout.Write($"{sheet.Name.PadLeft(sheetNamePadding)} : {$"R{r}C{dim.StartCol}".PadLeft(addrPad)} : ");

                        var firstCol = true;

                        for (int c = dim.StartCol; c <= dim.EndCol; c++)
                        {
                            var cell = sheet.Cells[r, c];
                            var columnWidth = columnWidths[cell.Start.Column];
                            string cellValue = GetCellText(cell).PadLeft(columnWidth + 1);

                            if (firstCol)
                                firstCol = false;
                            else
                                stdout.Write(",");

                            stdout.Write(cellValue);
                        }

                        stdout.WriteLine();
                    }                    
                }
            }
        }

        private static string GetCellText(ExcelRange cell)
        {
            return !string.IsNullOrWhiteSpace(cell.FormulaR1C1) ? "=" + cell.FormulaR1C1 : cell.Text;
        }

        //static void Main(string[] args)
        //{
        //    var stdout = Console.Out;

        //    using (var stdIn = Console.OpenStandardInput())
        //    {
        //        var book = new XSSFWorkbook(stdIn);
        //        foreach(var sheet in book.Sheets())
        //        {
        //            foreach(var row in sheet.Rows())
        //            {
        //                stdout.Write($"R{row.RowNum}C{row.FirstCellNum}".PadLeft(6) + " : ");

        //                var firstCol = true;

        //                foreach (var cell in row.Cells)
        //                {
        //                    var cellValue = cell.ToString();

        //                    switch(cell.CellType)
        //                    {
        //                        case CellType.Formula:
        //                            cellValue = "=" + cellValue;
        //                            break;
        //                    }

        //                    var columnWidth = sheet.GetColumnWidth(cell.ColumnIndex);

        //                    switch (sheet.GetColumnStyle(cell.ColumnIndex)?.Alignment ?? HorizontalAlignment.Left)
        //                    {
        //                        case HorizontalAlignment.Center:
        //                            cellValue = cellValue.PadLeft(columnWidth / 2).PadRight(columnWidth / 2 + 1);
        //                            break;

        //                        case HorizontalAlignment.Left:
        //                            cellValue = cellValue.PadRight(columnWidth);
        //                            break;

        //                        case HorizontalAlignment.Right:
        //                        default:
        //                            cellValue = cellValue.PadLeft(columnWidth);
        //                            break;
        //                    }


        //                    if (firstCol)
        //                        firstCol = false;
        //                    else
        //                        stdout.Write(",");

        //                    stdout.Write(cellValue);
        //                }

        //                stdout.WriteLine();
        //            }
        //        }
        //    }
        //}
    }
}
